#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <limits>

using namespace std;

struct Label {
	string name;
	int pos;
	explicit Label(string name, int pos): name(name), pos(pos) {};
};

string trim(const string& str, const string& whitespace=" \t") {
    int begin = str.find_first_not_of(whitespace);
    if (begin == string::npos) return "";
    int end = str.find_last_not_of(whitespace);
    int range = end - begin + 1;
    return str.substr(begin, range);
}

int get_label_pos(const vector<Label>& lbls, const string& s) {
	for(int i = 0; i < lbls.size(); ++i) {
		if(lbls[i].name == s) return lbls[i].pos;
	}
	return -1;
}

void run_script(vector<string> ll, vector<vector<int> >& grid, const vector<Label>& lbls) {
	int l = 0, r = 0, c = 0;
	int dir = 0; //0 = N, 1 = E, 2 = S, 3 = W
	while(l < ll.size()) {
		if(ll[l].find('j', 0) == 0) {
			int p = get_label_pos(lbls, ll[l].substr(5, ll[l].size()-5) + string(":"));
			if(p == -1) {
				cout << "[Error] Unrecognizable label \"" << ll[l].substr(5, ll[l].size()-5) << "\"" << endl;
				return;
			}else{
				l = p;
				continue;
			}
		}else if(ll[l].find('c', 0) == 0) {
			int p = get_label_pos(lbls, ll[l].substr(6, ll[l].size()-6) + string(":"));
			if(p == -1) {
				cout << "[Error] Unrecognizable label \"" << ll[l].substr(6, ll[l].size()-6) << "\"" << endl;
				return;
			}else{
				if(grid[r][c] > 0) l = p;
				else ++l;
				continue;
			}
		}else if(ll[l].find('e', 0) == 0) {
			int p = get_label_pos(lbls, ll[l].substr(5, ll[l].size()-5) + string(":"));
			if(p == -1) {
				cout << "[Error] Unrecognizable label \"" << ll[l].substr(5, ll[l].size()-5) << "\"" << endl;
				return;
			}else{
				switch(dir) {
					case 0:
						if(r-1 < 0) l = p;
						else ++l;
						break;
					case 1:
						if(c+1 > 255) l = p;
						else ++l;
						break;
					case 2:
						if(r+1 > 255) l = p;
						else ++l;
						break;
					case 3:
						if(c-1 < 0) l = p;
						else ++l;
						break;
				}
				continue;
			}
		}

		if(ll[l] == "left") {
			--dir;
			if(dir < 0) dir = 3;
		}else if(ll[l] == "right") {
			++dir;
			if(dir > 3) dir = 0;
		}else if(ll[l] == "move") {
			switch(dir) {
				case 0:
					if(r-1 < 0) break;
					--r;
					break;
				case 1:
					if(c+1 > 255) break;
					++c;
					break;
				case 2:
					if(r+1 > 255) break;
					++r;
					break;
				case 3:
					if(c-1 < 0) break;
					--c;
					break;
			}
		}else if(ll[l] == "put") {
			if(grid[r][c] < 15) ++grid[r][c];
		}else if(ll[l] == "get") {
			if(grid[r][c] > 0) --grid[r][c];
		}else if(ll[l] == "show") {
			for(int i = 0; i < 256; ++i)
				for(int j = 0; j < 256; ++j)
					if(grid[i][j] != 0)
						cout << "(" << i << ", " << j << ") = " << grid[i][j] << "\n";
		}else if(ll[l] == "pos") {
			cout << "Lucky = (" << r << ", " << c << ")" << endl;
		}else{
			cout << "[Error] Unrecognizable command \"" << ll[l].substr(0, ll[l].size()) << "\"" << endl;
			return;
		}

		++l;
	}
	cout << "Lucky ended up at (r, c): " << r << ", " << c << endl;
	return;
}

int main(int argc, char *argv[]) {

	vector<vector<int> > grid(256, vector<int>(256, 0));
	vector<Label> lbls;
	vector<string> ll;

	if(argc != 3) {
		cout << "Usage: " << argv[0] << " [filename].grd" << " [filename].lep" << endl;
	}else{
		ifstream lep(argv[2]);
		ifstream grd(argv[1]);
		if(!lep.is_open() && !grd.is_open()) {
			cout << "File(s) could not be opened!" << endl;
		}else{
			int r, c, lc;
			while(grd >> r) {
				grd >> c >> lc;
				grid[r][c] = lc;
			}
			grd.close();

			string input;
			int i = 0;
			while(getline(lep, input)) {
				input = trim(input);
				if(input.find('#', 0) == 0) {
					continue;
				}else if(input.find(':', 0) == input.size()-1) {
					lbls.push_back(Label(input, i));
				}else{
					ll.push_back(input);
					++i;
				}
			}

			lep.close();

			run_script(ll, grid, lbls);
		}
	}
	return 0;
}
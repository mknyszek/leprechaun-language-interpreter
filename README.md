# Leprechaun Language Interpreter #

An interpreter written in C++ for a limited assembly-like language that instructs Lucky the leprechaun to move about a 256x256 gridded board. Scripts in that language are included to solve problems based on the initial state of the board (lucky charms scattered across).

This project was created for a high school data structures class.

### Compile & Run ###

* Compile: 
g++ -o lep lep.cpp

* Run: 
./lep script[x].lep

### Leprechaun Language Specification ###

There are labels and instructions. Whitespace is ignored, but each instruction should be put on a separate line (although a label can go anywhere).

* Labels are denoted by ending in a colon (:), less than 128 characters in length.
* Lucky starts at row 0, column 0, counting from the northwest corner, facing south.
* Comments are denoted by a #
* On each point on the grid, there can be between 0 and 15 lucky charms.
* Lucky has an infinitely large bag of lucky charms with him at all times (he can place as many charms as he wants, and pick up as many as he wants).
* Valid instructions:
    * left - Have Lucky turn left.
    * right - Have Lucky turn right.
    * move - Have Lucky move in the direction he's facing (if on the edge, do nothing).
    * get - Pick up lucky charm.
    * put - Put down a lucky charm.
    * charm label - if there are >0 charms at the current point on the grid, go to label.
    * edge label - if on the edge, go to label.
    * jump label - jump to label.